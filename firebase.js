const admin = require("firebase-admin");
const serviceAccount = require("./firebase.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://start-1bd59.firebaseio.com"
});
module.exports = {
    createPerson: (email, pass) => {
        admin.auth().createUser({ email, pass })
            .then(userRecord => console.log('Successfully created new user:', userRecord.uid))
            .catch(err => console.log('Error creating new user:', err))
    },
    getPerson: (email) => {
        const verifyEmail = admin.auth().getUserByEmail(email)
            .then(() => true)
            .catch(() => false)
        return verifyEmail
    }
}