const app = require('express')()
const crypto = require('crypto')
const fuser = require('./firebase')

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

module.exports = {
    signin: function (req, _) {
        const pass = crypto.createHash('sha256').update(req.body.psw).digest("hex")
        fuser.createPerson(req.body.email, pass)
    },
    login: async function (req, _) {
        const verified = await fuser.getPerson(req.body.email)
        verified ? console.log("Loggined!") : console.log("not loggined!");
    }
}