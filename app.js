const express = require('express')
const app = express()
const path = require('path')
const user = require('./server')

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, './public')))

app.get('/', (_, res) => {
    res.sendFile(path.join(__dirname, 'public', 'login.html'))
})
app.get('/register', (_, res) => {
    res.sendFile(path.join(__dirname, 'public', 'reg.html'))
})
app.post('/', user.login)
app.post('/register', user.signin)

app.listen(3000, () => {
    console.log("server is listening 3000 port!");
})